<?php

namespace Checkout;

interface Cart
{
    /**
     * @param Item $item
     * @param int $qty
     */
    public function addItem(Item $item, $qty);

    /**
     * @return array
     */
    public function lines(): array;
}
