<?php

namespace Checkout;

interface Item
{
    /**
     * @param Item $item
     * @return boolean
     */
    public function equals(Item $item);

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function sku(): string;

    /**
     * @return float
     */
    public function price(): float;

    /**
     * @return bool
     */
    public function hasPercentageDiscount(): bool;

    /**
     * @return bool
     */
    public function hasPromoDiscount(): bool;

    /**
     * @return float
     */
    public function percentageDiscount(): float;
}
