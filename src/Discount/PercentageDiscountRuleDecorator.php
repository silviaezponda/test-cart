<?php

namespace Checkout\Discount;


use Checkout\Cart\Line;

class PercentageDiscountRuleDecorator extends DiscountRuleDecorator
{

    /**
     * @param Line $line
     * @return float
     */
    protected function calculate(Line $line): float
    {
        $basePrice = $line->item()->price() * $line->quantity();

        return $basePrice - ($basePrice * $line->item()->percentageDiscount());

    }

    /**
     * @param Line $line
     * @return bool
     */
    protected function applyRule(Line $line): bool
    {
        return $line->item()->hasPercentageDiscount();
    }
}