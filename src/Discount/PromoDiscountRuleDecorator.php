<?php

namespace Checkout\Discount;


use Checkout\Cart\Line;

class PromoDiscountRuleDecorator extends DiscountRuleDecorator
{
    const NUMBER_ITEMS_TO_BUY = 3;
    CONST NUMBER_ITEMS_TO_PAY = 2;

    /**
     * @param Line $line
     * @return float
     */
    protected function calculate(Line $line): float
    {
        $itemsFree = floor($line->quantity() / self::NUMBER_ITEMS_TO_BUY);
        $itemsWithPromo = $itemsFree * self::NUMBER_ITEMS_TO_PAY;
        $itemsWithoutPromo = $line->quantity() % self::NUMBER_ITEMS_TO_BUY;

        $price2 = ($itemsWithPromo * $line->item->price()) + ($itemsWithoutPromo * $line->item->price());

        return $price2;
    }

    /**
     * @param Line $line
     * @return bool
     */
    protected function applyRule(Line $line): bool
    {
        if (self::NUMBER_ITEMS_TO_BUY > $line->quantity()) {
            return false;
        }

        return $line->item()->hasPromoDiscount();
    }
}