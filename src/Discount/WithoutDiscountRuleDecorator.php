<?php

namespace Checkout\Discount;


use Checkout\Cart\Line;

class WithoutDiscountRuleDecorator extends DiscountRuleDecorator
{

    /**
     * @param Line $line
     * @return float
     */
    function calculate(Line $line): float
    {
        return $line->item()->price() * $line->quantity();
    }
}