<?php

namespace Checkout\Discount;

use Checkout\Cart\Line;

abstract class DiscountRuleDecorator
{
    /**
     * @var DiscountRuleDecorator
     */
    private $nextRule;

    /**
     * DiscountRuleDecorator constructor.
     * @param DiscountRuleDecorator|null $nextRule
     */
    public function __construct(DiscountRuleDecorator $nextRule = null)
    {
        $this->nextRule = $nextRule;
    }


    /**
     * @param Line $line
     * @return float
     */
    abstract protected function calculate(Line $line): float;

    /**
     * @param Line $line
     * @return float
     */
    public function checkout(Line $line): float
    {
        if ($this->applyRule($line)) {
            return $this->calculate($line);
        }

        return $this->next($line);
    }

    /**
     * @param Line $line
     * @return float
     */
    private function next(Line $line): float
    {
        if (!is_null($this->nextRule)) {
            return $this->nextRule->checkout($line);
        }

        return 0.0;
    }

    /**
     * @param Line $line
     * @return bool
     */
    protected function applyRule(Line $line): bool
    {
        return true;
    }
}