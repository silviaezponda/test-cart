<?php

namespace Checkout\Cart;

use Checkout\Cart;
use Checkout\Item;

class BasicCart implements Cart
{
    /**
     * @var array
     */
    private $lines;

    private function __construct()
    {
        $this->lines = [];
    }

    /**
     * @return BasicCart
     */
    public static function create()
    {
        return new self();
    }

    /**
     * @return array
     */
    public function lines(): array
    {
        return $this->lines;
    }

    /**
     * @param Item $item
     * @param int $qty
     * @throws InvalidQuantityException
     */
    public function addItem(Item $item, $qty)
    {
        if ($qty <= 0) {
            throw new InvalidQuantityException('Quantity must be higher than 0');
        }

        /** @var Line $line */
        foreach ($this->lines() as $index => $line) {
            if ($line->hasItem($item)) {
                $this->lines[$index] = $line->increaseQuantity($qty);
                return;
            }
        }

        $this->lines[] = Line::fromItemAndQuantity($item, $qty);
    }
}
