<?php

namespace Checkout\Cart;

use Checkout\Item;

class Line
{
    /** @var  \int */
    public $quantity;

    /** @var  Item */
    public $item;

    private function __construct(Item $item, int $quantity)
    {
        $this->item = $item;
        $this->quantity = $quantity;
    }

    /**
     * @param Item $item
     * @param int $quantity
     * @return Line
     */
    public static function fromItemAndQuantity(Item $item, int $quantity)
    {
        return new self($item, $quantity);
    }

    /**
     * @return int
     */
    public function quantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return Item
     */
    public function item(): Item
    {
        return $this->item;
    }

    /**
     * @param Item $item
     * @return bool
     */
    public function hasItem(Item $item): bool
    {
        return $this->item->equals($item);
    }

    /**
     * @param int $increase
     * @return Line
     */
    public function increaseQuantity(int $increase): Line
    {
        return self::fromItemAndQuantity($this->item(), $this->quantity() + $increase);
    }
}