<?php

namespace Checkout\Checkout;

use Checkout\Cart;
use Checkout\Checkout;
use Checkout\Cart\Line;
use Checkout\Discount\PercentageDiscountRuleDecorator;
use Checkout\Discount\PromoDiscountRuleDecorator;
use Checkout\Discount\WithoutDiscountRuleDecorator;

class BasicCheckout implements Checkout
{
    /**
     * @return BasicCheckout
     */
    public static function createBasicCheckout()
    {
        return new self();
    }

    /**
     * @param Cart $cart
     * @return float
     */
    public function calculate(Cart $cart): float
    {
        $result = 0.0;

        /** @var Line $line */
        foreach ($cart->lines() as $line) {
            $result += $this->calculateLinePrice($line);
        }

        return $result;
    }

    /**
     * @param Line $line
     * @return float
     */
    private function calculateLinePrice(Line $line): float
    {
        $basePrice = new WithoutDiscountRuleDecorator();
        $percentPrice = new PercentageDiscountRuleDecorator($basePrice);
        $promoPrice = new PromoDiscountRuleDecorator($percentPrice);

        return $promoPrice->checkout($line);
    }
}