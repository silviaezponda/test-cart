<?php

namespace Checkout\Item;

use Checkout\Item;

class BasicItem implements Item
{
    const FIVE_PERCENT_DISCOUNT = 0.05;
    const TEN_PERCENT_DISCOUNT = 0.1;
    const PERCENTAGE_DISCOUNT_ITEMS = ['AAA', 'BBB', 'DDD'];
    const PROMO_DISCOUNT_ITEMS = ['AAA', 'DDD', 'EEE'];
    const ITEMS = [
        'AAA' => 100,
        'BBB' => 55,
        'CCC' => 25,
        'DDD' => 25,
        'EEE' => 25
    ];

    /**
     * @var string
     */
    private $sku;

    /**
     * BasicItem constructor.
     * @param string $sku
     */
    public function __construct($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @param Item $item
     * @return boolean
     */
    public function equals(Item $item)
    {
        return $this->sku() === $item->sku();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->sku;
    }


    /**
     * @return string
     */
    public function sku(): string
    {
        return $this->sku;
    }

    /**
     * @return float
     * @throws ItemNotFoundException
     */
    public function price(): float
    {
        if (!isset(self::ITEMS[$this->sku])) {
            throw new ItemNotFoundException();
        }

        return self::ITEMS[$this->sku];
    }

    /**
     * @return bool
     */
    public function hasPercentageDiscount(): bool
    {
        return in_array($this->sku(), self::PERCENTAGE_DISCOUNT_ITEMS);
    }

    /**
     * @return bool
     */
    public function hasPromoDiscount(): bool
    {
        return in_array($this->sku(), self::PROMO_DISCOUNT_ITEMS);
    }

    public function percentageDiscount(): float
    {
        if ('BBB' === $this->sku()) {
            return self::FIVE_PERCENT_DISCOUNT;
        }

        return self::TEN_PERCENT_DISCOUNT;
    }
}
