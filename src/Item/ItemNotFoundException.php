<?php

namespace Checkout\Item;


class ItemNotFoundException extends \Exception
{

    /**
     * ItemNotFoundException constructor.
     */
    public function __construct()
    {
    }
}