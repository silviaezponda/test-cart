<?php
namespace Test\Checkout\Checkout;

use Checkout\Cart\BasicCart;
use Checkout\Checkout\BasicCheckout;
use Checkout\Item\BasicItem;
use PHPUnit\Framework\TestCase;

class BasicCheckoutTest extends TestCase
{
    /** @var  BasicCheckout */
    private $checkout;

    /** @var  BasicCart */
    private $cart;

    public function setUp()
    {
        $this->checkout = BasicCheckout::createBasicCheckout();
        $this->cart = BasicCart::create();
    }

    public function tearDown()
    {
        $this->checkout = null;
        $this->cart = null;
    }

    /**
     * @test
     */
    public function whenTheCartIsEmptyCalculationIsZero()
    {
        $expected = 0.0;
        $this->assertSame($expected, $this->checkout->calculate($this->cart));
    }

    /**
     * @test
     */
    public function whenAddItemWithoutDiscountToTheCartThePriceStays()
    {
        $itemWithoutDiscount = new BasicItem('CCC');
        $this->cart->addItem($itemWithoutDiscount, 1);

        $this->assertSame($itemWithoutDiscount->price(), $this->checkout->calculate($this->cart));
    }


    /**
     * @test
     */
    public function whenAddItemWithOnlyPercentageDiscountToTheCartOnlyThisDiscountIsApplied()
    {
        $itemWithOnlyPercentageDiscount = new BasicItem('BBB');
        $this->cart->addItem($itemWithOnlyPercentageDiscount, 1);

        $price = $itemWithOnlyPercentageDiscount->price() - ($itemWithOnlyPercentageDiscount->price() * $itemWithOnlyPercentageDiscount->percentageDiscount());
        $this->assertSame($price, $this->checkout->calculate($this->cart));
    }

    /**
     * @test
     */
    public function whenAddItemWithOnlyPromoDiscountToTheCartOnlyThisDiscountIsApplied()
    {
        $itemWithOnlyPromoDiscount = new BasicItem('EEE');
        $this->cart->addItem($itemWithOnlyPromoDiscount, 1);
        $this->assertSame($itemWithOnlyPromoDiscount->price(), $this->checkout->calculate($this->cart));

        $this->cart->addItem($itemWithOnlyPromoDiscount, 1);
        $this->cart->addItem($itemWithOnlyPromoDiscount, 1);
        $this->assertSame($itemWithOnlyPromoDiscount->price() * 2, $this->checkout->calculate($this->cart));
    }

    /**
     * @test
     */
    public function whenAddItemWithSeveralDiscountToTheCartOnlyOneIsApplied()
    {
        $itemWithVariousDiscounts = new BasicItem('AAA');
        $this->cart->addItem($itemWithVariousDiscounts, 1);

        // Percentage discount when no promo discount applied
        $priceWithPercentage = $itemWithVariousDiscounts->price() - ($itemWithVariousDiscounts->price() * $itemWithVariousDiscounts->percentageDiscount());
        $this->assertSame($priceWithPercentage, $this->checkout->calculate($this->cart));

        // Promo discount applied
        $this->cart->addItem($itemWithVariousDiscounts, 1);
        $this->cart->addItem($itemWithVariousDiscounts, 1);
        $this->assertSame($itemWithVariousDiscounts->price() * 2, $this->checkout->calculate($this->cart));

        // Until then only promo discount is applied
        $this->cart->addItem($itemWithVariousDiscounts, 1);
        $this->cart->addItem($itemWithVariousDiscounts, 1);
        $this->cart->addItem($itemWithVariousDiscounts, 1);
        $this->assertSame($itemWithVariousDiscounts->price() * 4, $this->checkout->calculate($this->cart));
    }
}