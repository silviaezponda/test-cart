<?php

namespace Tests\Unit\Checkout\Item;

use Checkout\Item\BasicItem;
use Checkout\Item\ItemNotFoundException;
use PHPUnit\Framework\TestCase;

class BasicItemTest extends TestCase
{
    /**
     * @test
     */
    public function whenRetrievingItemPriceItemIsNotFoundThrowAnException()
    {
        $this->expectException(ItemNotFoundException::class);
        $item = new BasicItem('foo');
        $item->price();
    }

    /**
     * @test
     */
    public function whenTwoItemsHaveSameSkuAreEquals()
    {
        $item = new BasicItem('AAA');
        $sameItem = new BasicItem('AAA');

        $this->assertTrue($item->equals($sameItem));
    }
}