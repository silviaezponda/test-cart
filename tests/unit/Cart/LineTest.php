<?php

namespace Test\Checkout\Cart;

use Checkout\Cart\Line;
use Checkout\Item\BasicItem;
use PHPUnit\Framework\TestCase;

class LineTest extends TestCase
{

    /** @var  Line */
    private $line;

    public function setUp()
    {
        $this->line = null;
    }

    /**
     * @test
     */
    public function checkLineContainsAnItem()
    {
        $item = new BasicItem('AAA');
        $this->line = Line::fromItemAndQuantity($item, 1);

        $this->assertTrue($this->line->hasItem($item));
    }
}