<?php
namespace Tests\Unit\Checkout\Cart;

use Checkout\Cart\BasicCart;
use Checkout\Cart\InvalidQuantityException;
use Checkout\Cart\Line;
use Checkout\Item\BasicItem;
use PHPUnit\Framework\TestCase;

class BasicCartTest extends TestCase
{
    /** @var BasicCart */
    private $basicCart;

    public function setUp()
    {
        $this->basicCart = BasicCart::create();
    }

    public function tearDown()
    {
        $this->basicCart = null;
    }

    /**
     * @test
     */
    public function whenInvalidQuantityRequestedThrownException()
    {
        $this->expectException(InvalidQuantityException::class);

        $item = new BasicItem('AAA');
        $this->basicCart->addItem($item, 0);
    }

    /**
     * @test
     */
    public function whenAddOneItemToCartShouldHaveOneLine()
    {
        $item = new BasicItem('AAA');
        $this->basicCart->addItem($item, 1);
        $this->assertEquals(1, count($this->basicCart->lines()));
    }

    /**
     * @test
     */
    public function whenAddSameItemToCartShouldHaveOneLineAndIncreaseQuantity()
    {
        $item = new BasicItem('AAA');
        $this->basicCart->addItem($item, 1);
        $this->basicCart->addItem($item, 5);
        $this->assertEquals(1, count($this->basicCart->lines()));

        /** @var Line $line */
        $line = current($this->basicCart->lines());
        $this->assertEquals(6, $line->quantity());
    }

    /**
     * @test
     */
    public function whenAddDifferentItemsToCartShouldHaveOneLineForEach()
    {
        $itemA = new BasicItem('AAA');
        $itemB = new BasicItem('BBB');
        $itemC = new BasicItem('CCC');

        $this->basicCart->addItem($itemA, 1);
        $this->basicCart->addItem($itemB, 1);
        $this->basicCart->addItem($itemC, 1);

        $this->assertEquals(3, count($this->basicCart->lines()));
    }
}