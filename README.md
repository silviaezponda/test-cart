Shopping Cart Checkout
======================

You do not need to write a full application, we only require that the integration tests runs.

##Problem

We need to implement a new shopping cart checkout process. This checkout is based on Lines
Each line has an item and a quantity (number of items). A requirement of the new checkout is that we must add the ability to calculate the price of every line based on some discount rules.

Only one discount rule is aplicable for each line, and they follow the following priority order:

- 3 x 2 (pay 2 get three)
- % discount
- by unit (no discount)

Current items with discount rules are as follow

|SKU|Discount Rule|Value|
|---|---|---|
|AAA|by unit|100 EUR|
||% discount|10%|
||3 X 2| - |
|BBB|by unit|55 EUR|
||% discount|5%|
|CCC|by unit|25 EUR|
|DDD|by unit|25 EUR|
||% discount|10%|
||3 X 2| - |


##Solution

This test includes an integration test that you cannot modify. To test only you need to execute:

```$ vendor/bin/phpunit tests/integration/ ```

Unit tests are required, please write them under:

```$ vendor/bin/phpunit tests/unit/ ```

###We will look positively to:

* The design of your unit tests
* Easy of change of the solution. 
* Coding principles used (SOLID, design patterns, architectural patterns ...) 


###Last reminder

You CANNOT modify the code in the integration test (BasicCheckoutTest). 
You can modify any class or interface supplied, organize the code it best suits to you.

But remember that to be successful, integration test must run:

```$ vendor/bin/phpunit tests/integration/ ```


